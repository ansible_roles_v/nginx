Nginx
=========
Installs and configures Nginx.  

Include role
------------
```yaml
- name: nginx 
  src: https://gitlab.com/ansible_roles_v/nginx/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - nginx
```