from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_files(host):
    files = [
        "/usr/sbin/nginx"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

def test_user(host):
    assert host.group("www-data").exists
    assert "www-data" in host.user("www-data").groups
    assert host.user("www-data").shell == "/usr/sbin/nologin"

def test_service(host):
    s = host.service("nginx")
    assert s.is_enabled
    assert s.is_running

def test_socket(host):
    sockets = [
        "tcp://127.0.0.1:80"
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
